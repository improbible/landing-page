if (('improbible.com' === window.location.host) && (window.location.protocol !== 'https:')) {
    window.location = window.location.toString().replace(/^http:/, 'https:');
} else if ('www.improbible.com' === window.location.host) {
    window.location = window.location.toString().replace(/^(http|https):\/\/www\.improbible\.com/, 'https://improbible.com');
} else if ('improbible.gitlab.io' === window.location.host) {
    window.location = window.location.toString().replace(/^(http|https):\/\/improbible\.gitlab\.io\/landing-page/, 'https://improbible.com');
}

